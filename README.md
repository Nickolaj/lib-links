# lib-links
url,bookmarks, link to book, documents, manuals

[tutorial](man/tutorial.md)

[php](man/php.md)

[markdown](man/markdown.md)

[docker](man/docker.md)

[git](man/git.md)

## Online

[DevDocs](https://devdocs.io/)
> DevDocs combines
> multiple API documentations
> in a fast, organized, and searchable interface.

### Laravel 

[awesome-laravel](https://github.com/chiraggude/awesome-laravel)
> A curated list of bookmarks, packages, tutorials, videos
> and other cool resources from the Laravel ecosystem 

[Laracasts](https://laracasts.com)
> Learn practical, modern web development, through expert screencasts.
> Most video tutorials are boring.

[The PHP Practitioner](https://laracasts.com/series/php-for-beginners)


### tools

[Adminer](https://www.adminer.org/en/)
> Database management in a single PHP file

[sqlectron](https://sqlectron.github.io/)
> A simple and lightweight SQL client desktop/terminal
> with cross database and platform support.
>> Available for Linux, Mac and Windows

[sqlectron-gui](https://github.com/sqlectron/sqlectron-gui/releases)
> A desktop application with a simple interface.

[sublimetext](https://www.sublimetext.com/)
> A sophisticated text editor for code, markup and prose

[symbol](./man/symbol.md)

### todo

[ToDo](man/todo.md)
