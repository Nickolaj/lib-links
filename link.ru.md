### PHP

[PHP podcast](https://5minphp.ru/)
> Подкаст о новостях из мира PHP,
> интересных постах в блогах
> и современных подходах к разработке

### Docker

[Шпаргалка с командами Docker #habr #Docker](https://habrahabr.ru/company/flant/blog/336654/)

[Мифы и рецепты Docker](https://habrahabr.ru/post/267441/)

[350+ полезных ресурсов, книг и инструментов](https://habrahabr.ru/company/1cloud/blog/275015/)

[Play with Docker](https://habrahabr.ru/company/flant/blog/334470/)

[Оптимизация образов Docker](https://habrahabr.ru/post/234829/)


### Utils

[Деплоим PHP-приложение с помощью deployer](https://phptoday.ru/post/deploim-php-prilozhenie-s-pomoshchyu-deployer)

[Готовим локальную среду Docker для разработки на PHP](https://phptoday.ru/post/gotovim-lokalnuyu-sredu-docker-dlya-razrabotki-na-php)

[Оптимизация загрузчика Composer](https://phptoday.ru/post/optimizaciya-zagruzchika-composer)

[Adminer — веб-интерфейс для баз данных размером в один .php файл](https://habrahabr.ru/post/268735/)


### Development with Docker, Laravel, Docker-Compose, Environment

[Docker: Installing Docker CE on Ubuntu 14.04 and 16.04](https://fabianlee.org/2017/03/07/docker-installing-docker-ce-on-ubuntu-14-04-and-16-04/)

[Laravel Development with Docker](https://kyleferg.com/laravel-development-with-docker/)

[PHP 7.1 Docker Compose Dev Environment](http://despairdrivendevelopment.com/php-71-docker-compose-dev-environment/)

[Docker in Development](https://serversforhackers.com/s/docker-in-development)
> A small course on using Docker Compose to get started using Docker now.
> We'll cover docker-compose, volumes, networks and more!

[Docker PHP Xdebug CLI debugging](https://sandro-keil.de/blog/2015/10/05/docker-php-xdebug-cli-debugging/)
