### Development with Docker, Laravel

[Laravel Development with Docker](https://kyleferg.com/laravel-development-with-docker/)

[PHP 7.1 Docker Compose Dev Environment](http://despairdrivendevelopment.com/php-71-docker-compose-dev-environment/)

[Docker in Development](https://serversforhackers.com/s/docker-in-development)
> A small course on using Docker Compose to get started using Docker now.
> We'll cover docker-compose, volumes, networks and more!

[Docker PHP Xdebug CLI debugging](https://sandro-keil.de/blog/2015/10/05/docker-php-xdebug-cli-debugging/)

[Developer-RoadMap](https://github.com/kamranahmedse/developer-roadmap)
> Roadmap to becoming a web developer in 2018 
