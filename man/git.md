### Git

[Git](https://git-scm.com/)
> Git is a free and open source
> distributed version control system
> designed to handle everything from small
> to very large projects with speed and efficiency.

[Git Ready](http://gitready.com/)
> learn git one commit at a time
> by Nick Quaranto

[External Links](https://book.git-scm.com/doc/ext)
> Tutorials, Books, Videos, Courses

##### `.gitignore`

[gitignore](https://git-scm.com/docs/gitignore)

[github/gitignore](https://github.com/github/gitignore)

[gitignore.io](https://www.gitignore.io/)
> Create useful .gitignore files for your project
