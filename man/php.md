### php

![](../img/php.png)

[Awesome-PHP](https://github.com/ziadoz/awesome-php)
> A curated list of amazingly awesome PHP libraries, resources and shiny things.

[Composer](https://getcomposer.org/)
> Dependency Manager for PHP

[Learn-php](https://github.com/odan/learn-php)
> Learn modern PHP

[laracasts:Recommended Reading](https://laracasts.com/recommended-reading)
> The reality is that, to make it in this industry,
> you must learn from a variety of resources.
> In the market for a new book? We recommend...
