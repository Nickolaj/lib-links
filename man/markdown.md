### Markdown

[Markdown-en.wiki](https://en.wikipedia.org/wiki/Markdown)

[Markdown-Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

[Markdown-Tutorial](https://www.markdowntutorial.com/)

[Markdown](https://blog.ghost.org/markdown/)

[Markdown-guide](https://guides.github.com/features/mastering-markdown/)
